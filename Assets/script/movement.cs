using Unity.Properties;
using UnityEngine;
using UnityEngine.UIElements;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private float jumpForce = 1000f;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;
    [SerializeField] private LayerMask GrLa;
    [SerializeField] private Collider2D feetCollider;
    private Rigidbody2D myrigidbody;
    private SpriteRenderer spriteRenderer;


    private void Awake()
    {
        myrigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        Flip(playerInput);
        bool isGrounded = feetCollider.IsTouchingLayers(GrLa);
        if (Input.GetKeyDown(jumpButton) && isGrounded) Jump();
    }

    private void Move(float direction)
    {
        Vector2 velectity = myrigidbody.velocity;
        myrigidbody.velocity = new Vector2(speed * direction, velectity.y);
    }

    private void Jump()
    {
        Vector2 jumpVecor = new Vector2(0f, jumpForce);
        myrigidbody.AddForce(jumpVecor);
    }

    private void Flip(float direction)
    {
        if (direction  > 0) spriteRenderer .flipX = false;
        if (direction < 0) spriteRenderer.flipX = true;
    }
        
    





}
