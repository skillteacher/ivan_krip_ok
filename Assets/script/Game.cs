using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public static Game game;
    [SerializeField] private int startLives = 2;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI coinText;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private TextMeshProUGUI finalCoinCount;
    [SerializeField] private string firstLevelName = "lvl1";
    private int lives;
    private int coins;
    private void Awake()
    {
        if (game == null)
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        lives = startLives;
        coins = 0;
        ShowLives();
        ShowCoins();
        mainMenu.SetActive(false);
        Time.timeScale = 1f;

    }
    public void ExitGame()
    {
        Application.Quit();

    }






    public void LoseLive()
    {
        lives--;
        ShowLives();
        if (lives <= 0) GameOver();
    }
    private void GameOver()
    {
        Time.timeScale = 0f;
        mainMenu.SetActive(true);
        finalCoinCount.text = coins.ToString();
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(firstLevelName);
        lives = startLives;
        ShowLives();
        ShowCoins();

    }







    public void AddCoins(int amount)
    {
        coins += amount;
        ShowCoins();
    }
    private void ShowLives()
    {
        livesText.text = lives.ToString();
    }


    private void ShowCoins()
    {
        coinText.text = coins.ToString();
    }

}


















































































































































































































































































































































































































































































































































