using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.Rendering;

public class coin : MonoBehaviour
{
    [SerializeField] private int coinCost = 1;
    [SerializeField] private LayerMask playerLayer;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var layer = 1 << collision.gameObject.layer;
        if ((layer & playerLayer) == 0) return;
        PickCoin();
    }
    private void PickCoin()
    {
        Game.game.AddCoins(coinCost);
        Destroy(gameObject);
    }
    






}
