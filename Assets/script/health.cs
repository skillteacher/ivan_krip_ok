using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health: MonoBehaviour
{
    private Vector3 startPosition;
    private void Start()
    {
        startPosition = transform.position;
    }


    


    public void TakeDamage()
    {
        transform.position = startPosition;
        Game.game.LoseLive();
    }
}
